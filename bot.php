<?php

include __DIR__.'/vendor/autoload.php';

set_error_handler(function ($errno, $errstr, $errfile, $errline) {
    throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
});

$bot = new WhumpBot\Bot();
$bot->register(new WhumpBot\Commands\EightBall());
$bot->register(new WhumpBot\Commands\CoinFlip());
$bot->register(new WhumpBot\Commands\PickSteamGame());
$bot->register(new WhumpBot\Commands\BadSteamGame());
$bot->register(new WhumpBot\Commands\RedditCommentLink());
$bot->run();
