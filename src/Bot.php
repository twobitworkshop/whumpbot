<?php

namespace WhumpBot;

use Discord\Discord;
use Dotenv\Dotenv;
//use Monolog\Logger as Monolog;

class Bot
{
    protected $commands;

    public static $client;

    public function __construct()
    {
        $env = new Dotenv(__DIR__.'/../');
        $env->load();
    }

    public function register(Command $command)
    {
        $this->commands[] = $command;
    }

    public function run()
    {
        $discord = new Discord([
            'token' => getenv('DISCORD_TOKEN'),
            //'loggerLevel' => Monolog::DEBUG
        ]);

        $discord->on('ready', function ($discord) {
            echo "Bot is ready!", PHP_EOL;

            // Listen for messages.
            $discord->on('message', function ($message, $discord) {
                echo "{$message->author->username}: {$message->content}",PHP_EOL;

                foreach ($this->commands as $command) {
                    if ($command->matchesSignature($message)) {
                        $command->run($message);
                    }
                }
            });
        });

        $discord->run();
    }
}
