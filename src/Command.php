<?php

namespace Whumpbot;

interface Command
{
    public function matchesSignature($message);
    public function run($message);
}
