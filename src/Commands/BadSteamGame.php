<?php

namespace WhumpBot\Commands;

use WhumpBot\Command;
use Goutte\Client as GoutteClient;
use DiscordPHP\Http\Http;
use GuzzleHttp\Client as GuzzleClient;
use Symfony\Component\DomCrawler\Crawler;

class BadSteamGame implements Command
{
    protected $minPage = 350;
    protected $maxPage = 540;
    protected $priceThreshold = 10;
    protected $storeSearchUrl = 'http://store.steampowered.com/search';

    public function matchesSignature($message)
    {
        $matches = null;
        $match = preg_match(
            '/pick a shitty steam game(?: for ([a-zA-Z0-9_]+))?(?: under \$?([0-9\.]+)(?: dollars|bucks)?)?/',
            $message->content,
            $matches);

        $this->price = isset($matches[2]) ? max(1, $matches[2]) : $this->priceThreshold;

        $this->forUser = isset($matches[1]) ? ($matches[1]) : 'Whumpies';
        return $match;
    }

    public function run($message)
    {
        echo 'here';
        try {
            $candidates = [];

            while (empty($candidates)) {
                $page = rand($this->minPage, $this->maxPage);

                $params = [
                    'sort_by=Reviews_DESC', // Get the bad stuff
                    'category1=998', // Only want games
                    'page='.$page,
                ];
                $url = $this->storeSearchUrl . '?' . implode('&', $params);

                $client = new GoutteClient();
                $crawler = $client->request('GET', $url);

                $crawler = $crawler
                    ->filter('.search_result_row')
                    ->each(function (Crawler $node) use (&$candidates) {
                        $review = $node
                            ->filter('.search_review_summary')->count();
                        $price = explode('$', $node->filter('.search_price')->text());
                        $price = (float)(trim(end($price)));
                        if ($review
                            && is_numeric($price)
                            && $price < $this->price
                            && $price != 0) {
                            $name = $node->filter('.title')->text();
                            $matches = null;
                            //if (preg_match('/app\/([0-9]+)/', $node->attr('href'), $matches)) {
                            //    $appid = $matches[1];
                                $candidates[] = [
                                    'name' => $name,
                                    'price' => $price,
                                    //'appid' => $appid,
                                    'link' => $node->attr('href'),
                                ];
                            //}
                        }
                    });
            }

            $game = $candidates[array_rand($candidates)];

            $message->channel->sendMessage(
                '_Pray for ' .
                $this->forUser .
                '_' .
                "\n" .
                $game['link']
            );

        } catch (\Exception $e) {
            echo get_class($e) . ': ' . $e->getMessage();
        }
    }
}
