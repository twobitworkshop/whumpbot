<?php

namespace WhumpBot\Commands;

use WhumpBot\Command;

class CoinFlip implements Command
{
    protected $messages = [
        'The coin flew off into the sunset.',
        'The coin landed on its edge.',
        'That\'s a rude thing to do to a coin.',
        'The south shall rise again, right Stephanie?',
        '',
    ];

    public function matchesSignature($message)
    {
        $matches = null;
        $match = preg_match(
            '/(f[a-z]+) a coin/',
            $message->content,
            $matches);

        $this->flipped = isset($matches[1]) ? (strtolower($matches[1]) == 'flip') : false;
        return $match;
    }

    public function run($message)
    {
        $response = rand(0, 1) ? 'Heads' : 'Tails';
        if (!$this->flipped) {
            $response = $response[array_rand($this->messages)];
        }
        $message->channel->sendMessage($response);
    }
}
