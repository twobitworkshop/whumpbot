<?php

namespace WhumpBot\Commands;

use WhumpBot\Command;

class EightBall implements Command
{
    protected $responses = [
        'It is certain',
        'It is decidedly so',
        'Without a doubt',
        'Yes definitely',
        'You may rely on it',
        'As I see it, yes',
        'Most likely',
        'Outlook good',
        'Yes',
        'Signs point to yes',
        'Reply hazy try again',
        'Ask again later',
        'Better not tell you now',
        'Cannot predict now',
        'Concentrate and ask again',
        'Don\'t count on it',
        'My reply is no',
        'My sources say no',
        'Outlook not so good',
        'Very doubtful',
        'Whumps down'
    ];

    public function matchesSignature($message)
    {
        return $this->endswith('??', $message->content);
    }

    public function run($message)
    {
        $response = $this->responses[array_rand($this->responses)];
        $message->channel->sendMessage($response);
    }

    protected function endswith($needle, $haystack)
    {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }

        return (substr($haystack, -$length) === $needle);
    }
}
