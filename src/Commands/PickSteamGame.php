<?php

namespace WhumpBot\Commands;

use WhumpBot\Command;

class PickSteamGame implements Command
{
    protected $steamNames = [];
    protected $vanityUrl = 'http://api.steampowered.com/ISteamUser/ResolveVanityURL/v0001/';
    protected $libraryUrl = 'http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/';
    protected $storeUrl = 'http://store.steampowered.com/api/appdetails/';

    public function matchesSignature($message)
    {
        $regex = '/pick a game for ([a-zA-Z0-9_]+(?: and [a-zA-Z0-9_]+)*)/';
        if (preg_match($regex, $message->content, $matches) === 1) {
            $this->steamNames = explode(' and ', $matches[1]);
            return true;
        }
        return false;
    }

    public function run($message)
    {
        $appIds = [];
        foreach ($this->steamNames as $steamName) {
            $steamIdResponse = $this->httpRequest($this->vanityUrl, [
                'key' => getenv('STEAM_KEY'),
                'vanityurl' => $steamName
            ]);
            if (!isset($steamIdResponse['response']['steamid'])) {
                $message->channel->sendMessage($steamName . ' was not found');
            }
            $steamId = $steamIdResponse['response']['steamid'];

            $libraryResponse = $this->httpRequest($this->libraryUrl, [
                'key' => getenv('STEAM_KEY'),
                'steamid' => $steamId
            ]);
            $games = $libraryResponse['response']['games'];

            // No games in list yet
            if (empty($appIds)) {
                foreach ($games as $game) {
                    $appIds[$game['appid']] = $game['appid'];
                }
            } else {
                $newAppIds = [];
                foreach ($games as $game) {
                    if (isset($appIds[$game['appid']])) {
                        $newAppIds[$game['appid']] = $game['appid'];
                    }
                }
                $appIds = $newAppIds;
            }
        }

        // pick a random game and replace it if it's not in the store
        // since some of the steam appids are weird and broken
        $appId = 0;
        $success = false;
        while (!$success && !empty($appIds)) {
            $appId = $appIds[array_rand($appIds)];
            unset($appIds[$appId]);

            $gameResponse = $this->httpRequest($this->storeUrl, [
                'appids' => $appId
            ]);

            $players = count($this->steamNames);
            $success = $gameResponse[$appId]['success']
                && (
                    ($players == 1 && $this->arraySearch($gameResponse[$appId]['data']['categories'], function ($elem) {
                        return $elem['description'] == 'Single-player';
                    }))
                    || 
                    ($players > 1 && $this->arraySearch($gameResponse[$appId]['data']['categories'], function ($elem) {
                        return $elem['description'] == 'Multi-player';
                    }))
                );
        }

        $name = $gameResponse[$appId]['data']['name'];
        $urlName = urlencode(str_replace(' ', '_', $name));

        $message->channel->sendMessage('https://store.steampowered.com/app/' . $appId . '/' . $urlName);

        //$message->channel->sendMessage($response);
    }

    protected function arraySearch($array, $callable)
    {
        foreach ($array as $elem) {
            if (call_user_func($callable, $elem) === true) {
                return $elem;
            }
        }
        return null;
    }

    protected function httpRequest($url, $params)
    {
        $ch = curl_init();
        curl_setopt_array($ch, [
            CURLOPT_URL => $url . '?' . implode('&', array_map(function ($key, $val) {
                return $key . '=' . $val;
            }, array_keys($params), $params)),
            CURLOPT_HEADER => false,
            CURLOPT_RETURNTRANSFER => true
        ]);
        return json_decode(curl_exec($ch), true);
    }
}
