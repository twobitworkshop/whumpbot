<?php

namespace WhumpBot\Commands;

use WhumpBot\Command;
use Goutte\Client as GoutteClient;
use DiscordPHP\Http\Http;
use GuzzleHttp\Client as GuzzleClient;
use Symfony\Component\DomCrawler\Crawler;

class RedditCommentLink implements Command
{
    protected $link;

    public function matchesSignature($message)
    {
        $matches = null;
        $match = preg_match(
            '/reddit\.com\/r\/[a-zA-Z_0-9-]+\/comments\/[a-z0-9]+\/[a-zA-Z_0-9-]+\/([a-z0-9]+)/',
            $message->content,
            $matches);

        $this->link = 'https://www.' . ($matches[0] ?? null);

        return $match;
    }

    public function run($message)
    {
        try {
            $goutte = new GoutteClient();
            $guzzle = new GuzzleClient(['verify' => false]);
            $goutte->setClient($guzzle);
            $crawler = $goutte->request('GET', $this->link);

            $comment = $crawler
                ->filter('.commentarea .entry')
                ->first();

            $author = $comment->filter('.author')->text();
            //$flair = $comment->filter('.flair')->text();
            $score = $comment->filter('.score.unvoted');
            $score = $score->count() ? $score->text() : 'score hidden';
            $comment = $comment->filter('.usertext-body')->text();

            $message->channel->sendMessage(
                '_Comment by ' . $author . ', ' . $score . '_' .
                "\n" .
                '```' . preg_replace('!\s+!', ' ', $comment) . '```'
            );

            // while (empty($candidates)) {
            //     $page = rand($this->minPage, $this->maxPage);
            //
            //     $params = [
            //         'sort_by=Reviews_DESC', // Get the bad stuff
            //         'category1=998', // Only want games
            //         'page='.$page,
            //     ];
            //     $url = $this->storeSearchUrl . '?' . implode('&', $params);
            //
            //     $client = new GoutteClient();
            //     $crawler = $client->request('GET', $url);
            //
            //     $crawler = $crawler
            //         ->filter('.search_result_row')
            //         ->each(function (Crawler $node) use (&$candidates) {
            //             $review = $node
            //                 ->filter('.search_review_summary')->count();
            //             $price = explode('$', $node->filter('.search_price')->text());
            //             $price = (float)(trim(end($price)));
            //             if ($review
            //                 && is_numeric($price)
            //                 && $price < $this->price
            //                 && $price != 0) {
            //                 $name = $node->filter('.title')->text();
            //                 $matches = null;
            //                 //if (preg_match('/app\/([0-9]+)/', $node->attr('href'), $matches)) {
            //                 //    $appid = $matches[1];
            //                     $candidates[] = [
            //                         'name' => $name,
            //                         'price' => $price,
            //                         //'appid' => $appid,
            //                         'link' => $node->attr('href'),
            //                     ];
            //                 //}
            //             }
            //         });
            // }
            //
            // $game = $candidates[array_rand($candidates)];
            //
            // $message->channel->sendMessage(
            //     '_Pray for ' .
            //     $this->forUser .
            //     '_' .
            //     "\n" .
            //     $game['link']
            // );

        } catch (\Exception $e) {
            echo get_class($e) . ' ' . $e->getLine() . ': ' . $e->getMessage();
        }
    }
}
