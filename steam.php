<?php

// include __DIR__.'/vendor/autoload.php';
//
// use Dotenv\Dotenv;
// use GuzzleHttp\Client;
// use GuzzleHttp\Psr7\Request;
// use Psr\Http\Message\ResponseInterface;
// use GuzzleHttp\Exception\RequestException;
// use GuzzleHttp\Promise;
//
// $env = new Dotenv(__DIR__);
// $env->load();
//
// $vanityUrl = 'http://api.steampowered.com/ISteamUser/ResolveVanityURL/v0001/';
//
// $client = new Client();
// $promises = [];
// $steamNames = [
//     'whumples',
//     'ark'
// ];
// foreach ($steamNames as $steamName) {
//     $promises[$steamName] = $client->getAsync($vanityUrl, [
//         'query' => [
//             'key' => getenv('STEAM_KEY'),
//             'vanityurl' => $steamName
//         ]
//     ])->then(function (ResponseInterface $response) {
//         var_dump(json_decode($response->getBody(), true));
//     }, function (RequestException $error) {
//         echo $error->getMessage();
//     });
// }
//
// $results = Promise\unwrap($promises);
